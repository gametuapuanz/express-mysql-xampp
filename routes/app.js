var express = require('express');
var router = express.Router();
const mysql = require('mysql')
const con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'sitepoint'
});

var connectDB = () => {
  con.connect((err) => {
    if (err) {
      console.log("Error connecting to DB");
      return;
    }
    console.log("Connection establishe");
  })

  return con;
}

/* GET home page. */
router.get('/', function(req, res, next) {
  let connect = connectDB();
  res.render('index.html', { message: 'Connection establishe' });
});
router.get('/authors', (req, res, next) => {
  let authors = con.query('SELECT * FROM authors', (err, rows) => {
    if (err) throw err;
    console.log({ data: rows })
    res.render('authors.html', { data: rows });
  });
});
router.get('/add', (req, res, next) => {
  res.render('add.html')
});
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.post('/add', (req, res, next) => {
  const author = req.body
  var lastId = con.query('INSERT INTO authors SET ?',
    author, (err, response) => {
      if (err) throw err
      console.log(response.insertId, author.name)
      res.render('show.html', { id: response.insertId });
    });

});

module.exports = router;